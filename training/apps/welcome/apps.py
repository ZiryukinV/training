from django.apps import AppConfig


class WelcomeConfig(AppConfig):
    name = 'training.apps.welcome'
